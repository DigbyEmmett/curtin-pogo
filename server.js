const express = require('express');
const app = express();
var cache = [];
var working = true;
const sql = require('sqlite3').verbose();
var createQuery = "CREATE TABLE IF NOT EXISTS LOCATIONS (lat FLOAT, lng FLOAT, nme TEXT, tpe TEXT,tme INT)"
/* initialise database if it doesnt already exist */
var db = new sql.Database("./data/pokemon.db",function(err){
    if(err!=null) {
        working = false;
        throw err;
    }
        db.run(createQuery,
          function(err)
          {
            if(err!=null)
            {
                throw err;
            }
          }
         );
    if(!working)
        console.log("Error initialising database")
});

app.use(express.static('public'));
app.get('/', function(request, response) {
  response.sendFile(__dirname + '/views/index.html');
});
app.post('/submit',function(request,response) {
    var p = request.query;
  db.run("INSERT INTO LOCATIONS VALUES (?,?,?,?,time('now'))",
         [p.lat,p.lng,p.nme,p.tpe],
         function(err){
            if(err){
              throw err;
            }
            console.log("UPDATING OTHER NODES");
            //send a update to alll connected nodes excluding this one
          }
        );
    console.log("Added",p.nme,"at",p.lat,",",p.lng);
});
app.get('/init',function(request,response) {
  db.all("SELECT * FROM LOCATIONS",[],(err,rows)=>{
    var mes = encodeURIComponent(JSON.stringify(rows));
    console.log(rows);
    response.send(mes);
  })
});
// listen for requests :)
const listener = app.listen(8080);
 //update the cache every 5 seconds using the records in the database
