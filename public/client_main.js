
var markers = {};
var timer;
isWaiting = false
name = ""
type = ""
var cache;
map = L.map('MAP').setView([-32.0066553,115.8917114],16)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  tap:true
}).addTo(map);
L.Control.Button = L.Control.extend({
	onAdd: function(map) {
		div = createControlBox()
		return div;
	},
	onRemove: function(map) {
		//do nothing
	}
});
L.control.button = function(opts) {
return new L.Control.Button(opts)
}
L.control.button({position:"bottomleft"}).addTo(map);
map.locate({watch:true})
map.on("locationfound",function(e) {
    if(markers["user"]==null) {
    markers["user"] = L.marker([e.latitude,e.longitude]).bindPopup("Your Location");
    map.addLayer(markers["user"]);
    }
    else
    {
        markers.user.setLatLng([e.latitude,e.longitude]);
    }
})
map.on("locationerror",function(e) {
    alert("Error Finding Location");
})
map.on("click",function(e) {
    if(e.originalEvent.target.parentElement.id!="ControlBox" && isWaiting) {
        coords = [e.latlng.lat,e.latlng.lng]
        createWaypoint(coords,name,type);
        isWaiting = false;
    }
})
document.body.onload=load
