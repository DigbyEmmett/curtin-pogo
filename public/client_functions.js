function parameterise(data) {
    return Object.entries(data).map(([key,val])=>key+'='+val).join('&');
}
function postAjax(data,loc){
  var xhr = new XMLHttpRequest();
  a = (parameterise(data));
  xhr.open("POST",loc+'?'+a,true);
  xhr.send();
}
function getAjax(data,loc) {
 var xhr = new XMLHttpRequest();
 xhr.open("GET",loc+'?'+parameterise(data),true);
 xhr.send();
}
function addMarker(la,lo,name) {
    markers[[la,lo].toString()] = L.marker({lat:la,lng:lo});
    markers[[la,lo].toString()].addTo(map).bindPopup(name).openPopup();
}
function createWaypoint(coords,name,type)
{
   /* extract the location from the event object and store it in the dataframe.
    do the same for the name but get that from the user. if the name is invalid then    we wont submit*/
    var la = coords[0]
    var lo = coords[1]
    var data = {lat:la,lng:lo,nme:name,tpe:type};
    addMarker(la,lo,name);
    postAjax(data,"/submit");
}
function submit(flag){
    name = prompt("Enter a name then tap where you would like to place the marker")
    isWaiting = true;
    type = flag
}
function addElement(type,data) {
	a = document.createElement(type);
    keys = Object.keys(data);
    for(i=0;i<keys.length;i++) {
        a[keys[i]] = data[keys[i]]
        console.log(keys[i],data[keys[i]]);
    }
    console.log(a)
    return a;
}
function createControlBox() {
	div= L.DomUtil.create("div");
	div.appendChild(addElement(
        "div",
        {
            innerText:"Add Pokestop",
            onclick:function(e) {submit("stop")},
        }))
	div.appendChild(addElement(
        "div",
        {
            innerText:"Add Gym",
            onclick:function(e) {submit("gym")},
        }))
    div.id="ControlBox";
	return div
}
function load(){
  var x = new XMLHttpRequest();
  x.onreadystatechange=function(e) {
    if(x.status==200 && x.readyState==4) {
      cache = JSON.parse(decodeURIComponent(x.responseText))
      for(i=0;i<cache.length;i++){
            var df = cache[i]
            a = L.marker([df.lat,df.lng]);
            markers[df.lat + "," + df.lng] = a;
            markers[df.lat + "," + df.lng].addTo(map).bindPopup(df.nme);
      }
    }
  }
  x.open("GET","/init",true);
  x.send();
}

